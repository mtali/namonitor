FROM r2enamonitor/namon:latest

LABEL version="1.0" maintainer="Maris Tali <maris.tali@cern.ch>"

COPY . /app
WORKDIR /app 

USER 1001

EXPOSE 8080

CMD ["python", "app.py"]


