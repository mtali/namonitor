from flask import Flask, render_template
import datetime, time
import os 
import jpype
import pytimber
import numpy as np
from scipy.optimize import curve_fit

# import pytimber

print("pytimber")

from flask_restful import Resource, Api, abort

app = Flask(__name__)
api = Api(app)

db = pytimber.LoggingDB()


@app.route('/')
@app.route('/index')
def index():
	return render_template('index.html')

conv_factor = 0.156

class Data(Resource):

	# Define model function to be used to fit to the data above:
	def gauss(self, x, *p):
	    A, mu, sigma = p
	    return A*np.exp(-(x-mu)**2/(2.*sigma**2))

	def fit_data(self, datax, datay, x):
		result = {}
		p0 = [np.max(datax), 0., 2.]

		for name, data in zip(['datax', 'datay'], [datax, datay]):
			try:
				coeff, var_matrix = curve_fit(self.gauss, x, data, p0=p0)
				fit = self.gauss(x, *coeff)
				result[name] = fit
				result[name+'coeff'] = np.abs(coeff)
			except Exception as e:
				result[name] = np.zeros(len(datax))
				result[name+'coeff'] = [0, 0, 0]
			
		return result

	def get(self, data_id):
		alldata = {
		'mwpc' : ['XBH8.XDWC.042.475:PROFILE', 'XBH8.XDWC.042.476:PROFILE'],
		'charge' : ['XBH8.XSCI.042.475:COUNTS', 'XBH8.XSCI.042.463:COUNTS'],
		'alive' : 'XBH8.XSCI.042.475:COUNTS'
		}

		variable = alldata[data_id]
		t2 = datetime.datetime.now()
		senddata = []

		if 'mwpc' in data_id:
			t1 = t2 - datetime.timedelta(minutes=15)
			pytimberdatax = db.get(variable[0], t2)
			pytimberdatay = db.get(variable[1], t2)
			times, datax =  pytimberdatax[variable[0]]
			times, datay =  pytimberdatay[variable[1]]
			datax = datax[-1]
			datay = datay[-1]

			x = list(np.arange(len(datax))/10.-5)
			fits = self.fit_data(datax, datay, x)


			senddata = list(zip(x, datax.tolist()))
			senddatay = list(zip(x, datay.tolist()))
			fitsx = list(zip(x, fits['datax'].tolist()))
			fitsy = list(zip(x, fits['datay'].tolist()))
			fitsxcoeff = list(fits['dataxcoeff'])
			fitsycoeff = list(fits['dataycoeff'])

			return {'datax' : senddata, 
			'datay' : senddatay,
			'fitx': fitsx,
			'fitxcoeff': fitsxcoeff,
			'fity': fitsy,
			'fitycoeff': fitsycoeff,
			'update' : str(datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S'))}

		elif 'charge' in data_id:
			t1 = t2 - datetime.timedelta(hours=2)
			pytimberdata = db.get(variable, t1, t2)
			times, data =  pytimberdata[variable[0]]
			times, data2 =  pytimberdata[variable[1]]
			
			if len(data) == 0:
				times = np.array([time.time()])
				data = np.array([0])
				data2 = np.array([0])

			#convert time to javascript time by *1000, add 60*60 s to get CET timezone from UTC
			timeslist = ((times+60*60)*1000).tolist()

			datalist = (data*conv_factor).tolist()
			data2list = (data2*conv_factor/3.).tolist()

			senddata = list(zip(timeslist, datalist))
			senddata2 = list(zip(timeslist, data2list))

			return {'data' : senddata,
			'data463' : senddata2, 
			'last' : "%.2e"%((data*conv_factor).tolist()[-1]), 
			'factor' : "%.2e"%np.array([conv_factor]), 
			'update' : str(datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S'))}
		elif 'alive' in data_id:
			pytimberdata = db.get(variable, t1, t2)
			times, data =  pytimberdata[variable]



api.add_resource(Data, '/api/data/<string:data_id>')


if __name__ == '__main__':
	print("Started container")
	app.run(host='0.0.0.0', port=8080)
