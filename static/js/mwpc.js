var sigma = '\u03C3'
var mu = '\u03BC'


/**
 * Custom selection handler that selects points and cancels the default zoom behaviour
 */
function selectPointsByDrag(e) {
    var sum = 0
    // Select points
    var series = this.series[0]

    Highcharts.each(series.points, function (point) {
        if (point.x >= e.xAxis[0].min && point.x <= e.xAxis[0].max &&
                point.y >= e.yAxis[0].min && point.y <= e.yAxis[0].max) {
            sum = sum + point.y
            // point.select(true, true);
        }
    });


    window.alert("Fluence "+ sum.toExponential(2) + " ions/cm2");

    return false; // Don't zoom
}


var options_charge = {
    chart: {
        renderTo: 'chargeplot',
        events: {
            load: function () {
                var chart = this;
                // set up the updating of the chart each second
                setInterval(function () {
                    $.getJSON(url, function(data){
                        update_charge(chart, data, 'Charge');
                    });
                    console.log(chart.subtitle.textStr)
                    chart.redraw();
                }, 30000);

            },
            selection: selectPointsByDrag
        },
        zoomType: 'xy'
    },
    title: {
        text: 'Beam Charge'
    },
    subtitle: {},
    lineWidth : 4,
    yAxis: [{
        title: {
            text: 'Ions/cm2/spill'
        },
        // max: 5000,
        min: 0,
        tickmarkPlacement: 'on',
        gridLineWidth: 1,
        labels: {formatter: function() {
                    return this.value.toExponential(2);
                }
            }
    }],
    xAxis: [{
        title: {
            text: 'Time'
        },
        tickmarkPlacement: 'on',
        type: 'datetime',
        gridLineWidth: 1,
    }],
    tooltip: {
                formatter: function() {
                  return '<span style="fill:'+this.series.color+'" >\u25CF</span> ' + Highcharts.dateFormat('%H:%M:%S : ',
                                          new Date(this.x)) +
                   this.y.toExponential(2);
                }
              },
    series: [{}, {}],
};

var options_mwpc = {
    chart: {
        renderTo: 'mwpcplot',
        events: {
            load: function () {
                var chart = this;
                var url_mwpc = "/api/data/mwpc"
                // set up the updating of the chart each second
                setInterval(function () {
                    $.getJSON(url_mwpc, function(data){
                        update_mwpc(chart, data, 'Charge');
                    });
                    console.log(chart.subtitle.textStr)
                    chart.redraw();
                }, 30000);
            }
        }
    },
    title: {
        text: 'Beam Profile'
    },
    subtitle: {},
    yAxis: [{
        title: {
            text: 'Voltage'
        },
        tickmarkPlacement: 'on',
        gridLineWidth: 1,
        // max: 2500,
        min: 0
    }],
    xAxis: [{
        title: {
            text: 'Position (cm)'
        },
        tickInterval:1,
        tickmarkPlacement: 'on',
        gridLineWidth: 1,
        max: 5,
        min: -5
    }],
    tooltip: {
                formatter: function() {
                  return '<span style="fill:'+this.series.color+'" >\u25CF</span> '+ Highcharts.numberFormat(this.x, 1) + 'cm : ' +
                    " " + Highcharts.numberFormat(this.y, 1) + ' V';
                }
              },
    series: [{}, {}, {}, {}]
};

var drawChart_charge = function (data, name) {
    var charge475 = {
        name:  "XSCI.042.475",
        data: data.data,
    };

    var charge463 = {
        name:  "XSCI.042.463",
        data: data.data463,
    };

    options_charge.series[0] = charge475;
    options_charge.series[1] = charge463;

    options_charge.subtitle['text'] = 'Last updated: ' + data.update;
    options_charge.title['text'] = 'Beam Charge : <b>' + data.last+'</b> ions/cm<sup>2</sup>/spill, factor: <b>'+ data.factor+'</b>';
    options_charge.subtitle['align'] = 'right';
    options_charge.subtitle['x'] = -10;

    var chart_charge = new Highcharts.Chart(options_charge);    
    return chart_charge
};

var update_charge = function (chart_charge, data, name) {
    var charge475 = {
        name:  "XSCI.042.475",
        data: data.data,
    };

    var charge463 = {
        name:  "XSCI.042.463",
        data: data.data463,
    };

    options_charge.series[0] = charge475;
    options_charge.series[1] = charge463;
    

    options_charge.subtitle['text'] = 'Last updated: ' + data.update;
    options_charge.title['text'] = 'Beam Charge : <b>' + data.last+'</b> ions/cm<sup>2</sup>/spill, factor: <b>'+ data.factor+'</b>';
    chart_charge.update(options_charge);
};


var drawChart_mwpc = function (data, name) {
    var names = ['Profile x', 'Profile y', 'Fit x', 'Fit y']
    var datas = [data.datax, data.datay, data.fitx, data.fity]
    var black = '#000000'
    var blue = '#7cb5ec'
    for (i = 0; i < names.length; i++) {
        var c = (i >= 2) ? black : blue;
        var dashS = (i >= 2) ? "longdash" : "solid";
        var nseriesData = {
            name: names[i],
            data: datas[i],
            color: c,
            dashStyle: dashS
        };
        options_mwpc.series[i] = nseriesData;
    } 

    options_mwpc.subtitle['text'] = 'Last updated: ' + data.update;
    options_mwpc.subtitle['align'] = 'right';
    options_mwpc.subtitle['x'] = -10;

    var chart_mwpc = new Highcharts.Chart(options_mwpc);

    return chart_mwpc
};

var update_mwpc = function (chart_mwpc, data, name) {
    var names = ['Profile x', 'Fit x', 'Profile y',  'Fit y']
    var datas = [data.datax, data.fitx, data.datay, data.fity]
    var black = '#000000'
    var blue = '#7cb5ec'
    for (i = 0; i < names.length; i++) {
        var c = (i >= 2) ? black : blue;
        var dashS = (i%2 == 1) ? "longdash" : "solid";
        var nseriesData = {
            name: names[i],
            data: datas[i],
            color: c,
            dashStyle: dashS
        };
        options_mwpc.series[i] = nseriesData;
    } 

    options_mwpc.subtitle['text'] = 'Last updated: ' + data.update;
    options_mwpc.title['text'] = 'Beam Profile : ' +mu+' <b>'+ data.fitxcoeff[1].toFixed(2) +'cm</b> '+sigma+' <b>'+ data.fitxcoeff[2].toFixed(2)+'cm</b>';
    chart_mwpc.update(options_mwpc)
};


var url = "/api/data/charge"
var url_mwpc = "/api/data/mwpc"

function updateChart(chart_mwpc, chart_charge) {
    $.getJSON(url, function(data){
        update_charge(chart_charge, data, 'Charge');
    });
    $.getJSON(url_mwpc, function(data){
         update_mwpc(chart_mwpc, data, 'Profile');
    });
}

var timer;
var refRate = 2000;
var usr = 'SPS1';
var vistarData = {"SPS1":{"name":"SPS Page 1","img":
"https:\/\/vistar-capture.web.cern.ch\/vistar-capture\/sps1.png","refreshrate":"1000","ext_refreshrate":"2000","lgbk":"1000"}};

function refresh(){
    var vistarImg = vistarData[usr].img;
    var refRate = vistarData[usr].refreshrate;

    var imgbg = $("#vistarImg").attr("src");
    $("#vistarImg").css( "background-image", 'url(' + imgbg + ')' );
    $("#vistarImg").attr("src", vistarImg + '?'+Math.random()).stop(true,true).hide().show();
    timer = setTimeout('refresh()', parseInt(refRate) );
};

$(document).ready(function() {
    var chart_charge = drawChart_charge({'data' :[]}, 'Charge');
    var chart_mwpc = drawChart_mwpc({'data' :[]}, 'Profile');

    updateChart(chart_mwpc, chart_charge);
});
