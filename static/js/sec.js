var options_sec = {
    chart: {
        renderTo: 'secplot',
        zoomType: 'x'
    },
    title: {
        text: 'Beam Intensity Monitoring'
    },
    subtitle: {},
    yAxis: [{
        title: {
            text: 'Intensity (p/spill)'
        },
        labels: {
            formatter: function () {
                return this.value.toExponential();
            }
        }
    }],
    xAxis: [{
        title: {
            text: 'Time'
        },
        type: 'datetime'
    }],
    tooltip: {
        formatter: function () {
            return ''+Highcharts.dateFormat('%A %e %b %Y - %H:%M:%S', new Date(this.x)) +'<span style="fill:'+this.series.color+'" >\u25CF</span> '+ this.y.toExponential(3) + ' p/spill <br><br>';
        },
    },
    series: []
};

var drawChart_sec = function (data, name) {
    var newSeriesData = {
        type: 'area',
        name: name,
        data: data.data
    };

    options_sec.series.push(newSeriesData);

    options_sec.subtitle['text'] = 'Last updated: ' + data.updated;
    options_sec.subtitle['align'] = 'right';
    options_sec.subtitle['x'] = -10;

    var chart_sec = new Highcharts.Chart(options_sec);
};

var data = [];

$(document).ready(function() {
    $.getJSON("../data/sec1.json", function(data){
        drawChart_sec(data, 'SEC1');
    });
});
